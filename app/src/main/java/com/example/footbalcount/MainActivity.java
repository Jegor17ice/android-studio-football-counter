package com.example.footbalcount;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.footbalcount.R;


public class MainActivity extends Activity {

    TextView counter1View;
    TextView counter2View;
    private Integer counter1 = 999;
    private Integer counter2 = 999;
    ImageButton btnBLue;
    ImageButton btnRed;
    Button btnReset;

    @SuppressLint("WrongViewCast")
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // найдем View-элементы
        counter1View = (TextView) findViewById(R.id.score_of_1_command);
        counter2View = (TextView) findViewById(R.id.score_of_2_command);
        btnRed = (ImageButton) findViewById(R.id.bottom_1);
        btnBLue = (ImageButton) findViewById(R.id.bottom_2);
        btnReset = (Button) findViewById(R.id.bottom_reset);
    }

    // Тут вставляем во вьюшки значения былые
    @Override

    public void onResume() {
        super.onResume();
        counter1View.setText(counter1.toString());
        counter2View.setText(counter2.toString());
    }

    // Создание обработчика
    @SuppressLint("ResourceType")

    public void onClick(View view) {

        // по id определеяем кнопку, вызвавшую этот обработчик
        switch (view.getId()) {
            case R.id.bottom_1:
                // кнопка RED
                counter1++;
                break;
            case R.id.bottom_2:
                // кнопка BLUE
                counter2++;
                break;
            case R.id.bottom_reset:
                counter1 = 0;
                counter2 = 0;
                break;
        }
        counter1View.setText(counter1.toString());
        counter2View.setText(counter2.toString());
    }

    //метод сохранения данных перед поворотом
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("count 1", counter1);
        outState.putInt("count 2", counter2);
    }

    //метод воостановления после поворота
    @Override
    public void onRestoreInstanceState(Bundle savedInstanteState) {
        super.onRestoreInstanceState(savedInstanteState);
        counter1 = savedInstanteState.getInt("count 1");
        counter2 = savedInstanteState.getInt("count 2");
    }


}